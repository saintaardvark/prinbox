package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/eduncan911/podcast"
	"github.com/urfave/cli"
)

// buildPodcast builds the RSS feed
func buildPodcast(c *cli.Context) {
	p := buildNewPodcast()
	files := getSortedFiles()
	for _, file := range files {
		if isMP3(file) == false {
			continue
		}
		fileDate := file.ModTime()
		description := buildDescription(file.Name())
		log.Printf("[DEBUG] Processing %s...\n", file.Name())
		item := podcast.Item{
			Title:       file.Name(),
			Description: description,
			ISubtitle:   "Foo",
			PubDate:     &fileDate,
		}
		item.AddEnclosure(urlArg+"/"+file.Name(), podcast.MP3, file.Size())
		if _, err := p.AddItem(item); err != nil {
			log.Printf(err.Error())
		}
	}
	log.Println("[DEBUG] And now the podcast: " + feedFile)
	f, err := os.Create(feedFile)
	defer f.Close()
	if err != nil {
		log.Printf("[ERROR] Can't open %s for writing: %s", feedFile, err.Error())
	}
	if err := p.Encode(f); err != nil {
		log.Printf("[ERROR] Error writing out podcast: %s", err.Error())
	}
}

// readFromFile reads a list of URLs from a file and returns them
func readFromFile(f string) ([]string, error) {
	content, err := ioutil.ReadFile(f)
	if err != nil {
		return nil, err
	}
	return strings.Split(string(content), "\n"), nil
}

// buildOutputArg builds the -o argument for youtube-dl
func buildOutputArg(dir string) (arg string) {
	arg = fmt.Sprintf("-o%s/%%(title)s.%%(ext)s", dir)
	return arg
}

// buildY2OCmd builds a youtube-dl command based on the URL
func buildY2OCmd(c *cli.Context, url string) (*exec.Cmd, error) {
	cmd := &exec.Cmd{}
	var err error
	if strings.Contains(url, "youtube") {
		cmd = exec.Command(y2o,
			"--extract-audio",
			"--audio-format",
			"mp3",
			"--write-description",
			"--restrict-filenames",
			buildOutputArg(outputDir),
			url)
	} else if strings.Contains(url, "vimeo") {
		cmd = exec.Command(y2o,
			"--extract-audio",
			"--audio-format",
			"mp3",
			"--write-description",
			"--restrict-filenames",
			buildOutputArg(outputDir),
			url)
	} else if strings.Contains(url, "mp3") {
		cmd = exec.Command("wget",
			"--header='Accept: text/html'",
			"--user-agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'",
			"--directory-prefix",
			outputDir,
			url)
	} else if url == "" {
		log.Println("[INFO] Skipping empty line")
		err = fmt.Errorf("Empty line")
	} else {
		log.Println("[INFO] I don't recognize that line, skipping it")
		err = fmt.Errorf("Unrecognized line")
	}
	return cmd, err
}

func buildDescription(file string) string {
	descriptionFileName := strings.Replace(file, ".mp3", ".description", 1)
	descriptionPath := fmt.Sprintf("%s/%s", outputDir, descriptionFileName)
	descriptionBytes, err := ioutil.ReadFile(descriptionPath)
	description := string(descriptionBytes)
	if err != nil {
		log.Printf("[DEBUG] Can't find description file [%s]: %s", descriptionPath, err.Error())
		description = "Description"
	}
	return description
}

func buildNewPodcast() podcast.Podcast {
	now := time.Now()
	p := podcast.New(
		"Prinbox",
		urlArg,
		"Aardvark listening",
		&now,
		&now,
	)
	p.AddImage(imgURL)
	return p
}

func getSortedFiles() []os.FileInfo {
	files, err := ioutil.ReadDir(outputDir)
	if err != nil {
		log.Fatal(err)
	}
	sort.Slice(files, func(i, j int) bool {
		iTime := files[i].ModTime()
		jTime := files[j].ModTime()
		return iTime.Before(jTime)
	})
	return files
}

func isMP3(file os.FileInfo) bool {
	return strings.Contains(file.Name(), "mp3")
}
