# Prinbox

Prinbox is a Small but Useful(tm) utility to turn Youtube videos into
an RSS feed, like an audio podcast.

# Usage

See `prinbox -h`.

# Note

- You'll need to edit the vars section in `prinbox.go` to customize this.
- Episode dates are derived from the modtime of the mp3

# TODO

- Allow for configuration file

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
