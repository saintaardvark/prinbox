package main

import (
	"os"

	"github.com/urfave/cli"
)

// TODO: Order feed entries by file mtime
// TODO: Be able to create output dir if needed
// TODO: Copy prinbox.jpg to output dir if needed, or be able to specify it in some way
// TODO: Episode images
// TODO: Better descriptions
// TODO: Return errors where appropriate
// DONE: Add image to podcast itself
// DONE: Use modtime of mp3 as episode date
// DONE: Be able to specify file as input file (ie, use mp3 instead of just youtube link)

var (
	y2o         = "youtube-dl"
	outputDir   = "/home/aardvark/public_html/random/prinbox"
	urlArg      = "https://saintaardvarkthecarpeted.com/random/prinbox"
	feedFile    = "/home/aardvark/public_html/random/prinbox/feed.xml"
	imgURL      = urlArg + "/prinbox.jpg"
	rebuildOnly = false
	inputfile   = ""
	urlList     = []string{}
)

func main() {
	app := cli.NewApp()
	app.Name = "prinbox"
	app.Copyright = "Copyright 2018 Hugh Brown and licensed under the GPL v3."
	app.Usage = "Make Podcast feed out of Youtube audio or random sound files"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "rebuild",
			Usage:       "Just rebuild the feed",
			Destination: &rebuildOnly,
		},
		cli.StringFlag{
			Name:        "feedfile",
			Usage:       "Where the output should go",
			Value:       feedFile,
			Destination: &feedFile,
		},
		cli.StringFlag{
			Name:        "inputfile",
			Usage:       "Path to a file with a list of URLs",
			Destination: &inputfile,
		},
	}
	app.Action = func(c *cli.Context) error {
		if rebuildOnly == false {
			grabAudio(c)
		}
		buildPodcast(c)
		return nil
	}
	app.Run(os.Args)
}
