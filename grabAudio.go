package main

import (
	"log"
	"math/rand"
	"os/exec"
	"time"

	"github.com/urfave/cli"
)

// grabAudio runs youtube-dl with the right options.
func grabAudio(c *cli.Context) {
	checkForPrereqs()
	var err error
	sleepPlease := false
	if inputfile != "" {
		if urlList, err = readFromFile(inputfile); err != nil {
			log.Println("[INFO] Can't open list of files: " + err.Error())
		}
	} else {
		if c.Args().Get(0) == "" {
			log.Println("[INFO] No URL, skipping download")
			return
		}
		log.Println("[INFO] Looks like I should be able to get " + c.Args().Get(0))
		urlList = append(urlList, c.Args().Get(0))
	}
	if len(urlList) > 1 {
		sleepPlease = true
		rand.Seed(time.Now().UTC().UnixNano())
	}
	for _, url := range urlList {
		log.Println("[INFO] Grabbing " + url)
		log.Println("[INFO] " + buildOutputArg(outputDir)) // output for debug
		cmd, err := buildY2OCmd(c, url)
		if err != nil {
			continue
		}
		if cmdOut, err := cmd.Output(); err != nil {
			log.Printf("[ERROR] Unknown error: %+v\n", err)
			log.Printf("[ERROR] Command was: %+v\n", cmd)
			log.Println("[ERROR] Output of : " + string(cmdOut) + string(err.(*exec.ExitError).Stderr))
			log.Printf("[ERROR] Output of %s: %s %s\n",
				string(cmd.Path),
				string(cmdOut),
				string(err.(*exec.ExitError).Stderr))
			log.Fatal(err)
		} else {
			log.Println("[INFO] " + string(cmdOut))
		}
		if sleepPlease == true {
			log.Println("[INFO] Sleeping a bit between fetches")
			time.Sleep(time.Duration(rand.Intn(15)) * time.Second)
		}
	}
}

// checkForPrereqs looks for the programs we need for youtube-dl
func checkForPrereqs() {
	if _, err := exec.LookPath("youtube-dl"); err != nil {
		log.Fatal("Can't find youtube-dl in PATH -- please install!\n")
	}
	if _, err := exec.LookPath("ffmpeg"); err != nil {
		if _, err := exec.LookPath("avconv"); err != nil {
			log.Fatal("Can't find ffmpeg or avconv in PATH -- please install!\n")
		}
	}
}
