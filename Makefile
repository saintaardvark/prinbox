SHORTVIDEO=https://www.youtube.com/watch?v=MkNeIUgNPQ8
MP3URL=http://cdn.media.ccc.de/congress/2017/mp3/34c3-9233-eng-Uncovering_British_spies_web_of_sockpuppet_social_media_personas.mp3

rebuild: prinbox prinbox.jpg
	./prinbox --rebuild

test:
	go run ./prinbox.go $(SHORTVIDEO)
	go run ./prinbox.go $(MP3URL)

prinbox: pull prinbox.go
	go build


.PHONY: pull
pull:
	git pull
